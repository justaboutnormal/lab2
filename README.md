# Lab 2

In this lab I will illustrate the usefulness of masks in combating the COVID-19 Pandemic.

# Demonstration

A demonstration of this project can be found on YouTube at [https://www.youtube.com/watch?v=vTLsxtwViVE](https://www.youtube.com/watch?v=vTLsxtwViVE).

# Instructions

## Installation

- Install Conda (as per instructions found [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)).
- download or clone this project
- navigate to the root of the download
- create a new conda environment
    - `conda create -n hi` (creates a new environment named `hi` (short for health informatics))
- install Jupiter dash (using conda)
    - `conda install -c conda-forge -c plotly jupyter-dash`
    - `pip install jupyterlab`
        - this prevented problems for me, and so I needed to follow the steps [here](https://github.com/plotly/jupyterlab-dash/issues/8#issuecomment-462135695) to install it locally for it to work namely:
            - from the root of _this_ project
            - `git clone https://github.com/plotly/jupyterlab-dash`
            - `cd jupyterlab-dash`
            - `npm build`
            - `npm run build`
            - `jupyter labextension link .`
            - ```conda install pip
                 pip install dash==0.35.1
                 pip install dash-html-components==0.13.4
                 pip install dash-core-components==0.42.1
                 pip install dash-table==3.1.11```
            - `python -m pip install -e .`
            - `npm install`
            - `npm run build`
            - `jupyter labextension link .`
    - `pip install pandas`
    - `pip install plotly` # this was installed as part of an earlier command, but I had problems with it and so I re-installed it with `pip`
    - `pip install dash==1.16.0`
    - `pip install dash-html-components==1.1.1`
    - `pip install dash-core-components==1.12.0`
    - `pip install dash-table==4.10.1`
    - `pip install jupyterlab`
    - `pip install chart_studio`
    - `pip install urllib`
    - `pip install urlopen`
    
## Running

The code for this is was created in a Jupyter Notebook. Assuming all dependencies are installed correctly you should run `juptyter notebook` from the root of the project then open `lab2.ipynb` in your browser.

The charts generated from the notebook are also included as html files which can simply be opened.